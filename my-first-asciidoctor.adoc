= My First GitLab and Asciidoctor
John Doe <johndoe@some.domain>
:doctype: book
:toc: left
:toclevels: 6
:icons: font

== Source

:gitlab-asciidoc-url: https://docs.gitlab.com/ee/user/asciidoc.html

[TIP]
.The GitLab Document
====
Please visit the {gitlab-asciidoc-url}[AsciiDoc| Gitlab], for further 
information about the *GitLab* & *AsciidDoctor* supports.
====

== The callout example

[source, java, linenums]
.The Java Hello World
----
package some.package;

public class Hello {
    public static void main(String[] args) {
        System.out.println("Hello World"); // <1>
    }
}
----
<1> Say hello

[,ruby]
.The Ruby Hello World
----
puts 'Hello, World!' # <1>
----
<1> Prints `Hello, World!` to the console.

== The table example

.The table with header
[cols="2*", options="header"]
|===
| Header 1
| Header 2

| Row 1, Col1
| Row 1, Col2

| Row 2, Col1
| Row 2, Col2

|===

.The frame none
[cols="2*", frame="none", options="header"]
|===
| Header 1
| Header 2

| Row 1, Col1
| Row 1, Col2

| Row 2, Col1
| Row 2, Col2

|===

.The grid none
[cols="2*", grid="none", options="header"]
|===
| Header 1
| Header 2

| Row 1, Col1
| Row 1, Col2

| Row 2, Col1
| Row 2, Col2

|===

.The grid none and frame none
[cols="2*", grid="none", frame="none", options="header"]
|===
| Header 1
| Header 2

| Row 1, Col1
| Row 1, Col2

| Row 2, Col1
| Row 2, Col2

|===
